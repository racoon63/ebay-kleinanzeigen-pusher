#!/usr/bin/env python3.7

"""
This file is the entrypoint for the EKP service.
"""

__authors__ = "Lenard Schottelius, Patrick Lubach, Ramon Dina"

# Imports
# Regular Imports


# Local Imports


# Global variables

# Class declarations

# Function declarations
def main():
    """
    Main function.
    """
    print("Hello World!")


# Entrypoint statement
if __name__ == '__main__':
    main()
